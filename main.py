import re, csv
import time
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory

factory1 = StopWordRemoverFactory()
stopword = factory1.create_stop_word_remover()

factory2 = StemmerFactory()
stemmer = factory2.create_stemmer()

def tokenWordbase(verse):
    return verse.split(' ')
	
#URLs
def url(link):
	link = re.sub(r'^https?:\/\/.*[\r\n]*', '', link, flags=re.MULTILINE)
	return link
	
#punctuation
def punctuation(tokens):
    tokens = re.sub(r'[>)}:{",?+ !.(<;1234567890]','',str(tokens))
    tokens = re.sub('\n','',str(tokens))
    return tokens

#emot
def emoticons(emot):
	emot = re.sub( ':-)', ':)', '(:', '(-:', ':-D', ':D', 'X-D', 'XD', 'xD', ';-)', ';)', ';-D', ';D', '(;', '(-;', ':-(', ':(', '(:', '(-:', ':,(', ':\'(', ':"(', ':((', str(tokens))
	return emot
	
def main():
	origin_time = time.time()
	currentString = []
	panjangTang = []
	with open('Book1.csv') as f:
		reader = csv.reader(f, delimiter=',')
        for row in reader:
            if row[0] == '' or row[0] == '-':
                continue
            else:
                tokennya = tokenWordbase(row[0])
                panjangTangSementara = []
                for j in range(len(tokennya)):
                    # print(childToken)
                    puntu = punctuation(tokennya[j])
                    thestop = stopword.remove(puntu)
                    if thestop == '':
                        continue
                    else:
                        thestem = stemmer.stem(thestop)
                        currentString.append(thestem)
                        panjangTangSementara.append(thestem)
                panjangTang.append(panjangTangSementara)
	daftarDok = Counter(currentString)
	daftarString = []
	for key, val in daftarDok.items():
		if key not in daftarString:
			daftarString.append(key) 
	print(daftarString)

	main()
